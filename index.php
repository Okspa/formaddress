<?php
require 'function.php';
//$product = new ProductDB() ;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Address</title>
	
   <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
	<link rel="stylesheet" href="css/style.css">
        <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <script type="text/javascript" src="js/function.js"></script>
         
</head>
<body>

    <div id="dialog_message" style="display: none;">
        <div id="code_message">
        </div>
    </div>
    
	<div class="container">
            
		<div class="row">
			<div class="col-md-12">
			<!--////////////////////-->
			
				<form method="post">
				  <div class="form-group">
					<label for="name">Ф.И.О</label>
					<input type="text" class="form-control" name="name" id="name" placeholder="Фамилия Имя Отчество">
				  </div>
				  <div class="form-group">
					<label for="sity_res">Город проживания </label>
					<input type="text" class="form-control" id="sity_res" name="sity_res" placeholder="Город проживания">
				  </div>
				  <div class="form-group">
					<label for="address_res">Адресс проживания</label>
					<input type="text" class="form-control" id="address_res" name="address_res" placeholder="Адресс проживания">
				  </div>
				  <div class="checkbox">
					<label>
					  <input type="checkbox"> Адресс проживания и адресс регистрации совпадают.
					</label>
				  </div>
				  <div class="form-group">
					<label for="sity_reg">Город регистрации </label>
					<input type="text" class="form-control" id="sity_reg" name="sity_reg" placeholder="Город проживания">
				  </div>
				  <div class="form-group">
					<label for="address_reg">Адресс регистрации</label>
					<input type="text" class="form-control" id="address_reg" name="address_reg" placeholder="Адресс регистрации">
				  </div>
				  <div class="form-group">
					<input type="hidden" id="ip" name="address_reg" value="<?= $_SERVER['REMOTE_ADDR']?>">
				  </div>
				  <button type="button" class="btn btn-default" onclick="javascript:saveAddress();">Отправить</button>
				</form>
	<!--////////////////////-->			
			</div>
		</div>
	</div>
</body>
</html>